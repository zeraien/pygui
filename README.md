Code from 2006 when I was trying to create a full UI library for pygame, it's ancient and doesn't quite work all the way, but it has some interesting concepts so I figured I'd put it up.

--

Some thoughts:

1. SpriteWidget+SpriteRenderer and ImageWidget+Renderer
These are all good and fine, however the problem is that the Widget is mostly obsolete.
The renderer becomes the imporant one. What is the widget used for? Keeping the position of the rendered image,
and obviously the metadata and other things about the widget.

One thing is that maybe renderers should extend widgets.... Although this means we wont be able to change
renderers on the fly.

2. Drawing and WindowManager
Need to look more into that, right now it's very messy.
In theory we can put an entire game area into a window, however this proved to be quite laggy.
More work is needed. Plebs are needed.

3. HTML XML
I am working on a parser, we will use XML to design the gui layouts rather then do it programmatically. That might 
make it easier.
How close to HTML should our XML files be? A copy or own idea?

4. Layouts
Using XML should be enough, however what we need is autosizing, deciding if things are going to be centered or not, 
and stuff like that. Panels within panels, next to eachother. Use some CSS ideas.

5. Text
Text input and output!
And selection also.

Good night.