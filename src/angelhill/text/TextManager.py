import pygame

class TextManager:
    def __init__(self,font, font_size,color=(255,255,255)):
        self.__doUpdate = True
        self.__text = ""
        self.__color=color
        self.__wrapto = 0
        if pygame.font:
            self.font = pygame.font.Font(font, font_size)

    def getSurface(self, wrapto = 0):
        if self.__doUpdate:
            if wrapto > 0:
                lines = self.wrapline(self.__text,wrapto)
                self.__text = "\n".join(lines)
                height = 0
                i = 0
                self.__image = None
                for line in lines:
                    currentLineSurface = self.font.render(line, 1, self.__color).convert_alpha()
                    height = currentLineSurface.get_rect().height
                    width = currentLineSurface.get_rect().width
                    if self.__image is None:
                        self.__image = pygame.Surface((width,height*len(lines)))
                        self.__image.set_alpha(0)
                        self.__image = self.__image.convert_alpha()
                    self.__image.blit(currentLineSurface,(0,i*height))
                    i+=1
                    
                    #print self.__text
            else:
                self.__image = self.font.render(self.__text , 1, self.__color).convert_alpha()

            self.__doUpdate = False
        return self.__image

    def setText(self,text):
        self.__text = text
        self.__doUpdate = True
    
    def setColor(self,color):
        self.__color = color
        self.__doUpdate = True
        
    def getSize(self):
        data = self.font.size(self.__text)
        width = data[0]
        height = data[1]
        #self.getSurface()
        #return (self.__image.get_rect().width,self.__image.get_rect().height)
        return (width, height)
    
    def truncline(self,text, maxwidth):
        font = self.font
        real=len(text)       
        stext=text           
        l=font.size(text)[0]
        a=0                  
        done=1               
        while l > maxwidth:  
                a=a+1        
                stext=text.rsplit(None, a)[0] 
                l=font.size(stext)[0]         
                real=len(stext)               
                done=0                        
        return real, done, stext             
    
    def wrapline(self,text, maxwidth): 
        font = self.font
        done=0                      
        wrapped=[]                  
                               
        while not done:             
                nl, done, stext=self.truncline(text, maxwidth) 
                wrapped.append(stext.strip())                  
                text=text[nl:]   
                                              
        return wrapped
