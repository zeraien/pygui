import pygame

class TextSprite(pygame.sprite.Sprite):
    def __init__(self,textManager):
        pygame.sprite.Sprite.__init__(self)
        self.textManager = textManager
        self.position = (0,0)
        self.rect = None
        self.update(0)

    def update(self,time):
        self.rect = self.image.get_rect()
        self.rect.center = self.position
