from .Widget import Widget
from .WindowManager import WindowManager
from .renderer import ButtonRenderer

class Button(Widget):
    """
    This is a button.
    
    TODO: we should be able to specify a size for our button!
    """
    
    def __init__(self, text, position, hint=None):
        super(Button, self).__init__(text=text,size=(0,0),position=position,hint=hint)

        self.setRenderer(ButtonRenderer(self))
        
        self.getWidgetProperties().setMovable(False)
        self.getWidgetProperties().setResizable(False)
        
        self.update()
        
    def addWidget(self,widget):
        raise NotImplementedError("A button can not have children.")
        
    def isActive(self):
        return WindowManager.instance.getSelectedWidget() == self
