from .Widget import Widget
from .Window import Window
from .WindowManager import WindowManager

from .renderer import DesktopRenderer

class Desktop(Widget):
    def __init__(self,name,size,position=(0,0), hint=None):
        super(Desktop, self).__init__(text=name, size=size, position=position, hint=hint)
        WindowManager.instance = WindowManager(self)
        self.setRenderer(DesktopRenderer(self))
        
        self.getWidgetProperties().setMovable(False)
        self.getWidgetProperties().setResizable(False)
        self.__windowList = dict()
    
    def addWidget(self,widget):
        """
        Overriden method from Widget. This method allows only Window instances to be added to the Desktop.
        If a Window instance is passed, the the superclass method is called, otherwise an exception is thrown.
        
        TODO: perhaps things like menu and files can also be added to the desktop
        """
        if not isinstance(widget,Window):
            raise AttributeError("Only instances of %s can be added to %s." % (Window.__name__,self.__class__.__name__))
        else:
            self.__windowList[widget.getTitle()] = widget
            super(Desktop, self).addWidget(widget)

    def getWindowList(self):
        return self.__windowList
    
    def getWindow(self,windowName):
        if windowName in self.__windowList:
            return self.__windowList[windowName]
