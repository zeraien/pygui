from .Widget import Widget
from .WindowManager import WindowManager
from .renderer import ButtonRenderer
from .Button import Button

class ImageButton(Button):
    """
    This is a button.
    
    TODO: we should be able to specify a size for our button!
    """

    def addWidget(self, widget):
        pass

    def __init__(self, text, position, hint=None):
        Button.__init__(self,text,(0,0),position,hint)
        
        self.setRenderer(ImageButtonRenderer(self))
        
        self.update()
