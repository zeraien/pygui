from .Widget import Widget
from .renderer import ImageRenderer

class ImageWidget(Widget):
    """
    This is the Image Widget. The idea is that it will take some sort of image and keep it obfuscated for use 
    in our PYGUI. If we are using pygame renderer, we should pass it a pygame surface object.
    """
    def __init__(self,name,surface,position=(0,0),hint=None):
        Widget.__init__(self,name,(0,0),position,hint)
        self.surface = surface
        self.setRenderer(ImageRenderer(self))
        self.getWidgetProperties().setMovable(False)
        self.getWidgetProperties().setResizable(False)
