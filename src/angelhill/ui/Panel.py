from .Widget import Widget
from .renderer import PanelRenderer

class Panel(Widget):
    def __init__(self,name,size,position=(0,0), hint=None):
        super(Panel, self).__init__(text=name, size=size, position=position, hint=hint)

        self.getWidgetProperties().setMovable(False)
        self.getWidgetProperties().setResizable(False)
        
        self.setRenderer(PanelRenderer(self))
