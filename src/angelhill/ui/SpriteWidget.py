from .Widget import Widget
from .renderer import SpriteRenderer

class SpriteWidget(Widget):
    """
    This is the Sprite Widget. The idea is that it will take some sort of sprite and keep it obfuscated for use 
    in our PYGUI. If we are using pygame renderer, we should pass it a sprite object.
    """
    def __init__(self,name,sprite,position=(0,0),hint=None):
        Widget.__init__(self,name,(0,0),position,hint)
        self.sprite = sprite
        self.setRenderer(SpriteRenderer(self))
        self.getWidgetProperties().setMovable(False)
        self.getWidgetProperties().setResizable(False)
