from .Widget import Widget
from .renderer import TextRenderer

class TextWidget(Widget):
    """A Text Widget, basically something that contains a lot of text."""
    
    def __init__(self,name,size,position=(0,0), hint=None):
        super(TextWidget, self).__init__(text=name, size=size, position=position, hint=hint)

        self.setRenderer(TextRenderer(self))
        
        self.getWidgetProperties().setMovable(False)
        self.getWidgetProperties().setResizable(False)
        self.text = ""
        self.update()

    def getText(self):
        return self.text
    
    def setText(self,text):
        self.text = text
