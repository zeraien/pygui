from .WidgetProperties import WidgetProperties
from angelhill.text.TextManager import TextManager
from .listeners import SelectionEvent
from .WindowManager import WindowManager
from .renderer import AbstractRenderer
from .listeners import AbstractSelectable

class Widget(AbstractSelectable):
    
    def __init__(self,text,size,position=(0,0),hint=None):
        AbstractSelectable.__init__(self)
        self.__hint = hint
        self.__movePointerOffset = position
        self.__title = text
        self.__size = size
        self.__widgets = list()
        self.__position = position
        self.__parent = None
        self.__widgetProperties = WidgetProperties()
        self.__renderer = None
        
        self.forceRepaint = True
        self.moved = True

    
    def getRenderer(self):
        return self.__renderer
    
    def setRenderer(self,renderer):
        if not isinstance(renderer,AbstractRenderer):
            raise AttributeError("Renderer must be subclass of angelhill.ui.renderer.AbstractRenderer")
        else:
            self.__renderer = renderer
          
    def getDisplay(self):
        """Return the Display instance that this widget is using. This is not the same as the renderer, but
        is usually tightly connected to the renderer."""
        return WindowManager.instance

    def getTitle(self):
        return self.__title
    def setTitle(self,_text):
        self.__title = _text
        self.forceRepaint = True
    
    def addWidget(self,widget):
        self.__widgets.append(widget)
        self.forceRepaint = True
        widget.__parent = self
    
    def getChildren(self):
        return self.__widgets
    
    def getParent(self):
        return self.__parent
    
    def setHint(self, text):
        self.__hint = text
        self.forceRepaint = True
        
    def getHint(self):
        return self.__hint
       
    def checkSelection(self):
        """
        Check if this widget has been clicked on.
        If it has, make sure that only the topmost widget recieves the click and 
        process that widget's selection listeners.
        TODO: Should the widget that received the click be returned?
        TODO: How do we process things like window motion or button click-animations?
        TODO: clicking fast on a widget can pass the click to the widget under it, hard to reproduce on a fast comp, seems to be a performance issue
        print "Widget.py reports: testing for collision",self,"coords and rect",coords,self.rect
        """
        doClickAction = False

        if self.getDisplay().isPointerOverWidget(self):
            doClickAction = True
            i = len(self.__widgets)-1
            while i >= 0:
                widget = self.__widgets[i]
                i -= 1
                if widget.checkSelection():
                    return True
            
        if doClickAction:
            self.getDisplay().setSelectedWidget(self)
            print("Widget.py reports: Click action performed on",self)
            self.notifySelectionListeners(self, self.getDisplay().getPointerPosition())
        
        return doClickAction                
                
    def setSize(self,size):
        self.__size = size
        self.forceRepaint = True
        
    def getSize(self):
        return self.__size
    
    def getWidgetProperties(self):
        return self.__widgetProperties
    
    def setPosition(self,position):
        """
        Set widget position.
        @param position list(x,y)
        """
        self.__position = position
        self.moved = True
        self.forceRepaint = True
        
    def getPosition(self):
        return self.__position
    
    def repaint(self,forceRepaint=True):
        """
        Call the update method on our renderer. If forceRepaint parameter is True, then force a 
        repaint of the component, wether it wants to or not.
        """
        if self.getRenderer() is not None:
            self.getRenderer().update(forceRepaint)

    def __repr__(self):
        return "<%s, title: %s, position: %s>" % (self.__class__.__name__, self.getTitle(), str(self.getSize()))

    def update(self):
        # TODO: This should only be called once i guess.... For example if a window moves, then a buttom's moved flag is set to true, so this might be called too much
        if self.moved or self.forceRepaint:
            #TODO: this method call is pygame dependant
            self.getDisplay().regenSpriteGroup()
        
        if self.__parent is not None and self.__parent.moved == True:
            parentpos = self.__parent.getPosition()
            widgetOffset = self.__movePointerOffset
            self.setPosition((parentpos[0]+widgetOffset[0],parentpos[1]+widgetOffset[1]))
            
        for widget in self.__widgets:
            #if widget.forceRepaint == True:
            #    self.forceRepaint = True
            widget.update()
        
        self.repaint(self.forceRepaint)
        self.forceRepaint = False
        self.moved = False
