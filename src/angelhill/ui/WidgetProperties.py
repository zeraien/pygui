class WidgetProperties:
    def __init__(self):
        self.resizable = False
        self.movable = False
    def setResizable(self, r):
        self.resizable = r
    def isResizable(self):
        return self.resizable
    def setMovable(self,m):
        self.movable = m
    def isMovable(self):
        return self.movable