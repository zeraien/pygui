from .Widget import Widget
from .renderer import WindowRenderer

class Window(Widget):
    r"""A window Widget"""
    
    def __init__(self,name,size,position=(0,0), hint=None):
        super(Window, self).__init__(text=name,size=size,position=position,hint=hint)

        self.setRenderer(WindowRenderer(self))
        
        self.getWidgetProperties().setMovable(True)
        self.getWidgetProperties().setResizable(True)
        
        self.update()

    def checkSelection(self):
        """
        This method raises the window by appending it to the top of the widget list.
        
        TODO: This is a problem since graphically the window is raised, but internally the window is lowered.
        SOLVED: by iterating through the windows backwards when checking which window is clicked
        """
        sel = super(Window, self).checkSelection()
        self.moved = True
        if sel and self.getParent() is not None:
            self.getParent().getChildren().remove(self)
            self.getParent().getChildren().append(self)
        return sel
    
    def addWidget(self,widget):
        if isinstance(widget,Window):
            raise AttributeError("Instances of Window can NOT be added to Windows.")
        else:
            Widget.addWidget(self,widget)
