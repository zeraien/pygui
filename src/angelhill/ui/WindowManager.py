import pygame
from pygame.locals import *

class WindowManager:
    """
    This is the WindowManager. It keeps an instance of the desktop and does things like process events and 
    decides(partially) who is clicked on and such.
    TODO: this is pygame dependant, parts of it should be moved into a renderer subsystem
    """
    instance:'WindowManager' = None

    def __init__(self,desktop):
        self.__desktop = desktop
        self.__pointerPosition = (0,0)
        self.__buttonState = dict()
        self.__selectedWidget = None
        self.__moveDiff = (0,0)
        self.__spriteGroup = pygame.sprite.OrderedUpdates()

    def regenSpriteGroup(self):
        self.__spriteGroup.empty()
        self.__addToSpriteGroup(self.__desktop)
        
    def __addToSpriteGroup(self,widget):
        if widget.getRenderer() != None:
            self.__spriteGroup.add(widget.getRenderer())
        for child in widget.getChildren():
            self.__addToSpriteGroup(child)
            
    def getSpriteGroup(self):
        """Return our spritegroup, can be used to check collisions and such."""
        return self.__spriteGroup
            
    def clear(self,background):
        """Clean all of this Desktop's sprites from our background, and replace it with a new background."""
        self.getSpriteGroup().clear(self.__desktop.getRenderer().image,background)

    def draw(self):
        """Draw our Windows and stuff onto our background."""
        self.getSpriteGroup().draw(self.__desktop.getRenderer().image)
 
    def setBackground(self,surface):
        """Set a background surface. This should usually be the bottom-most screen surface itself."""
        self.__desktop.getRenderer().image = surface      

    def processEvent(self,pygameEvent):
        if hasattr(pygameEvent, 'button'):
            self.__setButtonState(pygameEvent.button,pygameEvent.type)
    
        if pygameEvent.type == MOUSEMOTION:
            self.__setPointerPosition(pygameEvent.pos)
            
        if pygameEvent.type == MOUSEBUTTONDOWN:
            self.__desktop.checkSelection()
        
    def __setPointerPosition(self,coords):
        self.__pointerPosition = coords

        # should this be here or in a widgets update method?
        if self.getSelectedWidget() != None and self.getSelectedWidget().getWidgetProperties().isMovable():
            newpos = coords
            self.getSelectedWidget().setPosition((newpos[0]-self.__moveDiff[0],newpos[1]-self.__moveDiff[1]))
        
    def getPointerPosition(self):
        return self.__pointerPosition
    
    def __setButtonState(self,buttonNr,state):
        self.__buttonState[buttonNr] = state
        if not self.isDefaultButtonPressed():
            self.setSelectedWidget(None)
        
    def isDefaultButtonPressed(self):
        return self.isButtonPressed(1)
        
    def isButtonPressed(self,buttonNr):    
        if buttonNr in self.__buttonState and self.__buttonState[buttonNr] == MOUSEBUTTONDOWN:
            return True
        else:
            return False
    
    def isPointerOverWidget(self,widget):
        return widget.getRenderer().rect.collidepoint(self.getPointerPosition())
        
    def getSelectedWidget(self):
        return self.__selectedWidget
    
    def setSelectedWidget(self,widget):
        tmp = self.__selectedWidget
        self.__selectedWidget = widget
        if tmp != None:
            tmp.forceRepaint = True
        if widget != None:
            self.__moveDiff = self.__pointerPosition[0]-widget.getPosition()[0], self.__pointerPosition[1]-widget.getPosition()[1]
            widget.forceRepaint = True
        else:
            self.__moveDiff = (0,0)     
