
class SelectionEvent:
    """
    Selection event is created when an object is selected/clicked.
    It contains the source object that was clicked and the position of the click.
    """
    def __init__(self,source,pos):
        self._source = source
        self._position = pos
        
    def getSource(self):
        return self._source
    def getEventPosition(self):
        return self._position

class SelectionListener:
    """
    SelectionListener is implemented by something that wants to process a selection event.
    A selection event is when something is deemed "selected" or clicked, like a button.
    Add the implementation to the widget when you want the action to be performed if the
    widget is selected.
    """
    def widgetSelected(self, selectionEvent):
        raise NotImplementedError(str(self)+"widgetSelected(...) Must be overridden")

class AbstractSelectable:
    def __init__(self):
        self.__selectionListeners = list()
        
    def addSelectionListener(self,selectionListener):
        if not isinstance(selectionListener,SelectionListener):
            raise AttributeError("Only instances of %s can be added to an %s."%(SelectionListener.__name__,self.__class__.__name__))
        else:
           self.__selectionListeners.append(selectionListener)
    
    def notifySelectionListeners(self, widget, coords):
        for selectionListener in self.__selectionListeners:
            selectionListener.widgetSelected(SelectionEvent(widget,coords))