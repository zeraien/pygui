import pygame
from .skin import Skin

class AbstractRenderer(pygame.sprite.Sprite):
    skin = None
    def __init__(self, widget):
        super(AbstractRenderer, self).__init__()
        self.updated = False
        self.setWidget(widget)
        if AbstractRenderer.skin is None:
            AbstractRenderer.skin = Skin()
        
        self.__skinProperties = AbstractRenderer.skin.getProperties()
    
    def getWidget(self):
        return self.__widget
    
    def setWidget(self,widget):
        self.__widget = widget
    
    def getSkinProperties(self):
        return self.__skinProperties
    
    def getSurface(self):
        return self.image
    
    def forceUpdate(self):
        """Abstract method: This method must be overriden by a superclass."""
        raise NotImplementedError(str(self)+": Method draw now implemented. Must be overriden by a superclass.")
    
    def update(self,force=False):
        if force or self.updated == False or self.__skinProperties.needUpdate():
            self.forceUpdate()
            self.__skinProperties.doUpdate(False)
        
        self.rect.center = self.getWidget().getPosition()
