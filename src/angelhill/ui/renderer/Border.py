import pygame

#TODO: Should Border be a static object that is used by multiple items?
#Perhaps a Border pool for same-size windows?

class Border:
    """
    A border around a window or widget.
    Params: int weight, list size (x,y), list color (r,g,b), int opacity (0-255)
    
    Call UIBorder.draw(self,surface) to draw the border onto a surface.
    """
    def __init__(self,weight,size,color,opacity=255):
        self._weight = weight
        self._size = size
        self._vBorder = pygame.Surface((weight,size[1]))
        self._vBorder.fill(color)
        self._vBorder.set_alpha(opacity)
        self._hBorder = pygame.Surface((size[0]-(weight*2),weight))
        self._hBorder.fill(color)
        self._hBorder.set_alpha(opacity)
        """
        self.image = pygame.Surface(size)
        self.image.set_alpha(0)
        self.image = self.image.convert_alpha()
        self.image.blit(vBorder,(0,0))
        self.image.blit(vBorder,(size[0]-weight,0))
        self.image.blit(hBorder,(weight,0))
        self.image.blit(hBorder,(weight,size[1]-weight))
        self.image = self.image.convert_alpha()
        """
        
    def draw(self,surface):
        """
        Blit borders onto a surface.
        TODO:this draws borders on the inside of the surface, should it be changed? or should the caller worry about that?
        """
        surface.blit(self._vBorder,(0,0))
        surface.blit(self._vBorder,(self._size[0]-self._weight,0))
        surface.blit(self._hBorder,(self._weight,0))
        surface.blit(self._hBorder,(self._weight,self._size[1]-self._weight))
    