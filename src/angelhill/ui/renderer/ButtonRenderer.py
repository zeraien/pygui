import pygame
from .Shadow import Shadow
from .Border import Border
from .AbstractRenderer import AbstractRenderer

class ButtonRenderer(AbstractRenderer):
    def __init__(self,button):
        super(ButtonRenderer, self).__init__(button)

    def forceUpdate(self):
        
        buttonActive = self.getWidget().isActive()
        buttonText = self.getWidget().getTitle()
        
        self.getSkinProperties().getTextManager().setText(buttonText)
            
        size = self.getSkinProperties().getTextManager().getSize()
        size = (size[0]+8, size[1]+4)
        self.getWidget().setSize(size)
        
        self.image = pygame.Surface(size)
        self.image.fill(self.getSkinProperties().getWidgetColor())
        self.image.set_alpha(self.getSkinProperties().getWidgetOpacity())
        
        self.image = self.image.convert_alpha()
        border = Border(self.getSkinProperties().getBorderWeight(),size,self.getSkinProperties().getBorderColor(),self.getSkinProperties().getBorderOpacity())
        border.draw(self.image)
        
        self.image.blit(self.getSkinProperties().getTextManager().getSurface(),(4,2))
        
        self.rect = self.image.get_rect()
        
        size = self.rect.width+self.getSkinProperties().getShadowDistance(),self.rect.height+self.getSkinProperties().getShadowDistance()
        
        if ( buttonActive ):
            self.tmp = pygame.Surface(size)
            self.tmp.set_alpha(0)
            self.tmp = self.tmp.convert_alpha()
            self.tmp.blit(self.image,(self.getSkinProperties().getShadowDistance(),self.getSkinProperties().getShadowDistance()))
        else:
            shadow = Shadow(self.getSkinProperties().getShadowDistance(),self.rect.size,(0,0,0),self.getSkinProperties().getWidgetOpacity())
            self.tmp = shadow.draw(self.image)
                
        self.image = self.tmp.convert_alpha()
        
        self.updated = True
