import pygame
from .AbstractRenderer import AbstractRenderer

class DesktopRenderer(AbstractRenderer):
    def __init__(self,desktop):
        super(DesktopRenderer, self).__init__(desktop)
        self.image = pygame.Surface(self.getWidget().getSize())
        self.rect = self.image.get_rect()
        
    def forceUpdate(self):
        
        self.updated = True
