import pygame
from .AbstractRenderer import AbstractRenderer

class ImageRenderer(AbstractRenderer):
    def __init__(self,anImageWidget):
        if not isinstance(anImageWidget.surface,pygame.Surface):
            raise AttributeError("%s needs %s, but recieved a %s" % (self.__class__.__name__,pygame.Surface.__name__,anImageWidget.surface.__class__.__name__))
        else:
            AbstractRenderer.__init__(self,anImageWidget)
        
        self.imageObject = self.getWidget().imageObject
        self.update()
        self.getWidget().setSize(self.rect.size)
    
    def setSurface(self,surface):
        self.imageObject = surface
        self.update()
    
    def update(self,force=False):
        self.image = self.imageObject
        self.rect = self.imageObject.get_rect()
        self.rect.center = self.getWidget().getPosition()
        
    def forceUpdate(self):
        self.updated = True
