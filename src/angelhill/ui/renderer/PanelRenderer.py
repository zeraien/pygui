import pygame
from .AbstractRenderer import AbstractRenderer
from .Border import Border
from .Shadow import Shadow

class PanelRenderer(AbstractRenderer):
    def __init__(self,aPanel):
        super(PanelRenderer, self).__init__(aPanel)

    def forceUpdate(self):
        panelTitle = self.getWidget().getTitle()
        panelSize = self.getWidget().getSize()
        
        if panelTitle is not None:
            self.getSkinProperties().getTextManager().setText(panelTitle)
            textHeight = self.getSkinProperties().getTextManager().getSize()[1]
        
        content = pygame.Surface(panelSize)
        content.fill(self.getSkinProperties().getWidgetColor())
        content.set_alpha(self.getSkinProperties().getWidgetOpacity())
        
        border = Border(
            weight=self.getSkinProperties().getBorderWeight(),
            size=panelSize,
            color=self.getSkinProperties().getBorderColor(),
            opacity=self.getSkinProperties().getBorderOpacity()
        )
        border.draw(content)
        
        #shadow = Shadow(self.getSkinProperties().getShadowDistance(),(windowSize[0], windowSize[1]+titlebar.get_rect().height),(0,0,0),self.getSkinProperties().getWidgetOpacity())
        #self.image = shadow.draw(self.image)
        
        if panelTitle is not None:
            content.blit(self.getSkinProperties().getTextManager().getSurface(),(4,0))
        
        self.image = content.convert_alpha()
        self.rect = self.image.get_rect()
        
        self.updated = True

"""

        self.image = pygame.Surface((windowSize[0],windowSize[1]+textHeight+4))
        self.image.set_alpha(0)
        self.image = self.image.convert_alpha()
        self.image.blit(windowContent,(0,titlebarHeight))
        self.image.blit(titlebar,(0,0))
        
"""
