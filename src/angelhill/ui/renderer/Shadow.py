import pygame

#TODO: Should Shadow be a static object that is used by multiple items?
#Perhaps a shadow pool for same-size windows?

class Shadow:
    def __init__(self,distance,size,color,opacity=255):
        self.__distance = distance
        self.__color = color
        self.__opacity = opacity
        self.__size = size
        self.__vShadow = pygame.Surface((distance,size[1]))        
        self.__vShadow.set_alpha(opacity)
        self.__vShadow.fill(color)
        self.__hShadow = pygame.Surface((size[0]-distance,distance))
        self.__hShadow.set_alpha(opacity)        
        self.__hShadow.fill(color)        
        
    def draw(self,surface):
        newSurface = pygame.Surface((self.__size[0]+self.__distance,self.__size[1]+self.__distance))
        newSurface.set_alpha(0)
        newSurface = newSurface.convert_alpha()
        newSurface.blit(surface,(0,0))
        
        newSurface.blit(self.__hShadow,(self.__distance,self.__size[1]))
        newSurface.blit(self.__vShadow,(self.__size[0],self.__distance))
        return newSurface