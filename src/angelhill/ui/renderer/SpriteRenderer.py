import pygame
from .AbstractRenderer import AbstractRenderer

class SpriteRenderer(AbstractRenderer):
    def __init__(self,aSpriteWidget):
        if not isinstance(aSpriteWidget.sprite,pygame.sprite.Sprite):
            raise AttributeError("%s needs %s, but recieved a %s" % (self.__class__.__name__,pygame.sprite.Sprite.__name__,aSpriteWidget.sprite.__class__.__name__))
        else:
            AbstractRenderer.__init__(self,aSpriteWidget)
        
        self.imageObject = self.getWidget().sprite
        self.update()
        self.getWidget().setSize(self.rect.size)
    
    def update(self,force=False):
        self.imageObject.update(pygame.time.get_ticks())
        self.image = self.imageObject.image
        self.rect = self.imageObject.image.get_rect()
        self.rect.center = self.getWidget().getPosition()
        
    def forceUpdate(self):
        self.updated = True
