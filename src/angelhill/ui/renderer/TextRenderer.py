import pygame
from .AbstractRenderer import AbstractRenderer
from .Border import Border
from .Shadow import Shadow

class TextRenderer(AbstractRenderer):
    def __init__(self,textWidget):
        AbstractRenderer.__init__(self,textWidget)
        
    def forceUpdate(self):
        text = self.getWidget().getText()
        size = self.getWidget().getSize()
        
        self.getSkinProperties().getTextManager().setText(text)
        textHeight = self.getSkinProperties().getTextManager().getSize()[1]

        widgetContent = pygame.Surface(size)
        widgetContent.fill(self.getSkinProperties().getWidgetColor())
        widgetContent.set_alpha(self.getSkinProperties().getWidgetOpacity())
        
        self.image = pygame.Surface((size[0],size[1]))
        self.image.set_alpha(0)
        self.image = self.image.convert_alpha()
        self.image.blit(widgetContent,(0,0))
        
        border = Border(self.getSkinProperties().getBorderWeight(),size,self.getSkinProperties().getBorderColor(),self.getSkinProperties().getBorderOpacity())
        border.draw(self.image)
        
        #shadow = Shadow(self.getSkinProperties().getShadowDistance(),size,(0,0,0),self.getSkinProperties().getWidgetOpacity())
        #self.image = shadow.draw(self.image)
        
        self.image.blit(self.getSkinProperties().getTextManager().getSurface(size[0]),(4,2))
        
        self.image = self.image.convert_alpha()
        self.rect = pygame.Rect(0,0,size[0],size[1])
        
        self.updated = True
