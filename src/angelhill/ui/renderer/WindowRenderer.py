import pygame
from .AbstractRenderer import AbstractRenderer
from .Border import Border
from .Shadow import Shadow

class WindowRenderer(AbstractRenderer):
    def __init__(self,window):
        super(WindowRenderer, self).__init__(window)

    def forceUpdate(self):
        windowTitle = self.getWidget().getTitle()
        windowSize = self.getWidget().getSize()
        
        self.getSkinProperties().getTextManager().setText(windowTitle)
        textHeight = self.getSkinProperties().getTextManager().getSize()[1]
        
        titlebar = pygame.Surface((windowSize[0],textHeight+4))
        titlebar.fill(self.getSkinProperties().getTitlebarColor())
        titlebar.set_alpha(self.getSkinProperties().getTitlebarOpacity())
        titlebarHeight = textHeight+4
        
        windowContent = pygame.Surface(windowSize)
        windowContent.fill(self.getSkinProperties().getWidgetColor())
        windowContent.set_alpha(self.getSkinProperties().getWidgetOpacity())
        
        self.image = pygame.Surface((windowSize[0],windowSize[1]+textHeight+4))
        self.image.set_alpha(0)
        self.image = self.image.convert_alpha()
        self.image.blit(windowContent,(0,titlebarHeight))
        self.image.blit(titlebar,(0,0))
        
        border = Border(
            weight=self.getSkinProperties().getBorderWeight(),
            size=(windowSize[0], windowSize[1]+titlebarHeight),
            color=self.getSkinProperties().getBorderColor(),
            opacity=self.getSkinProperties().getBorderOpacity()
        )
        border.draw(self.image)
        
        shadow = Shadow(self.getSkinProperties().getShadowDistance(),(windowSize[0], windowSize[1]+titlebar.get_rect().height),(0,0,0),self.getSkinProperties().getWidgetOpacity())
        self.image = shadow.draw(self.image)
        
        self.image.blit(self.getSkinProperties().getTextManager().getSurface(),(4,2))
        
        self.image = self.image.convert_alpha()
        self.rect = pygame.Rect(0,0,windowSize[0],windowSize[1]+titlebarHeight)
        
        self.updated = True
