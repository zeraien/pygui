from .SkinProperties import SkinProperties
from angelhill.text.TextManager import TextManager
#TODO: There is still no direct idea of what and how to deal with skins and such...
class Skin:
    def __init__(self):
        self._properties = SkinProperties((100,100,100), TextManager("data/fonts/vera.ttf",12), 120, 1, (0,255,255), 150,(0,0,20),200,4)
        
    def setProperties(self,_properties):
        self._properties = _properties
        
    def getProperties(self):
        return self._properties
