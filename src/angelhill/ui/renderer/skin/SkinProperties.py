
class SkinProperties:
    def __init__(self,widgetColor,textManager,widgetOpacity=255,borderWeight=1,borderColor=(255,255,255),borderOpacity=255, titlebarColor=(0,0,0), titlebarOpacity=255,shadowDistance=4):
        self._widgetColor = widgetColor
        self._widgetOpacity = widgetOpacity
        self._textManager=textManager
        self._doUpdate= False
        self._borderOpacity = borderOpacity
        self._borderColor = borderColor
        self._borderWeight = borderWeight
        self._titlebarColor = titlebarColor
        self._titlebarOpacity = titlebarOpacity
        self._shadowDistance = shadowDistance
    def setShadowDistance(self,distance):
        self._shadowDistance = distance
    def getShadowDistance(self):
        return self._shadowDistance
    def setTitlebarOpacity(self,opacity):
        self._titlebarOpacity = opacity
    def getTitlebarOpacity(self):
        return self._titlebarOpacity
    def setTitlebarColor(self,color):
        self._titlebarColor = color
    def getTitlebarColor(self):
        return self._titlebarColor
    def setTextManager(self,textManager):
        self._textManager = textManager
        self._doUpdate= True
    def getTextManager(self):
        return self._textManager
    def setBorderWeight(self,weight):
        self._borderWeight = weight
        self._doUpdate= True
    def getBorderWeight(self):
        return self._borderWeight
    def setBorderOpacity(self,opacity):
        self._borderOpacity = opacity
        self._doUpdate= True
    def getBorderOpacity(self):
        return self._borderOpacity
    def setBorderColor(self,color):
        self._borderColor = color
        self._doUpdate= True
    def getBorderColor(self):
        return self._borderColor
    def setWidgetOpacity(self,opacity):
        self._widgetOpacity = opacity
        self._doUpdate= True
    def getWidgetOpacity(self):
        return self._widgetOpacity
    def setWidgetColor(self,color):
        self._widgetColor = color
        self._doUpdate= True
    def getWidgetColor(self):
        return self._widgetColor
    def needUpdate(self):
        return self._doUpdate
    def doUpdate(self,update):
        self._doUpdate = update