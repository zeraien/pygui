import pygame, sys,os, math
from pygame.locals import *
import thingSprite,graphics,angelhill.ui

pygame.init()
resolution = (800,600)
center = (resolution[0]/2,resolution[1]/2)
window = pygame.display.set_mode(resolution)
pygame.display.set_caption('Py.sWM')
screen = pygame.display.get_surface()
clock = pygame.time.Clock()

frame = thingSprite.UIFrame("Hello")
radar = thingSprite.AnimatedSprite(graphics.Animation(graphics.ImageLoader().loadImages('data/graphics/radar'),16))
ship = thingSprite.AnimatedSprite(graphics.Image(graphics.ImageLoader().loadImage('data/graphics/ship-02.png')))

"""
UI generation code:
"""
desktop = angelhill.ui.Desktop("Angelhill",resolution,center)
window1 = angelhill.ui.Window("Window 1",(200,200),(100,100))
window2 = angelhill.ui.Window("Window 2",(230,130),(50,50))
window3 = angelhill.ui.Window("Window 3",(200,100),(200,50))
window4 = angelhill.ui.Window("Text Window",(200,100),(400,50))
textWidget = angelhill.ui.TextWidget("Some text",(190,90),(0,8))
textWidget.setText("More text that becomes quite long as things progress. This should be wrapped.")
panel = angelhill.ui.Panel("Panel",(180,90),(0,8))
radarWidget = angelhill.ui.SpriteWidget("Radar",radar)
shipWidget = angelhill.ui.SpriteWidget("Ship",ship)
shipWidget.getWidgetProperties().setMovable(True)
panel.addWidget(radarWidget)
window2.addWidget(shipWidget)
button = angelhill.ui.Button("A Button",(0,30))
window3.addWidget(panel)
desktop.addWidget(window1)
desktop.addWidget(window2)
desktop.addWidget(window3)
desktop.addWidget(window4)
window4.addWidget(textWidget)
window1.addWidget(button)

bg_surface = pygame.image.load(os.path.join("data","graphics","stars-01.gif")).convert()

screen.blit(bg_surface,(0,0))
desktop.getDisplay().setBackground(screen)

class Selected(angelhill.ui.listeners.SelectionListener):
    def widgetSelected(self,selectionEvent):
        print("############# Click has been registered!",selectionEvent.getSource(), selectionEvent.getEventPosition())

button.addSelectionListener(Selected())

frame_group = pygame.sprite.RenderUpdates((frame,))

while True:
    deltat = clock.tick(20)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit(0)
        if hasattr(event, 'key'):
            if event.key == K_ESCAPE: sys.exit(0)
            
        desktop.getDisplay().processEvent(event)
   
    ship.direction += 10
    desktop.getDisplay().clear(bg_surface)
    desktop.update()
    desktop.getDisplay().draw()
    frame_group.update(0)
    frame_group.draw(screen)
    
    pygame.display.flip()
    
server.close()
