import os, pygame

class ImageLoader:
    images = dict()
    
    def __init__(self):
        pass

    @staticmethod
    def loadImage(image):
        if image not in ImageLoader.images:
            ImageLoader.images[image] = pygame.image.load(image).convert_alpha()
        return ImageLoader.images[image]
        
    @staticmethod
    def loadImages(imageDir):
        if imageDir not in ImageLoader.images:
            files = os.listdir(imageDir)
            files.sort()
            ImageLoader.images[imageDir] = list()
            for file in files:
                if file[0] != ".":
                    imageFileName = os.path.join(imageDir,file)
                    image = pygame.image.load(imageFileName).convert_alpha()
                    ImageLoader.images[imageDir].append(image)
    
        return ImageLoader.images[imageDir]
            
        
class Image:
    def __init__(self,image):
        self._image = image
        
    def getImage(self):
        return self._image

class Animation(Image):
    
    def __init__(self, images, fps = 10):
        self._images = images
        self._delay = 1000 / fps
        self._last_update = 0
        self._frame = 0
        self._num_frames = len(images)
        super(Animation, self).__init__(images[0])

    def getImage(self):
        current_t = pygame.time.get_ticks()
        if current_t - self._last_update > self._delay:
            self._frame += 1
            if self._frame >= self._num_frames:
                self._frame = 0
            self._image = self._images[self._frame]
            self._last_update = current_t

        return self._image
