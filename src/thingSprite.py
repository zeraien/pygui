import pygame
import graphics

class TextSprite(pygame.sprite.Sprite):
    def __init__(self,font, font_size,color=(255,255,255)):
        super(TextSprite, self).__init__()
        self.text = ""
        self.color=color
        self.position = (0,0)
        if pygame.font:
            self.font = pygame.font.Font(font, font_size)

    def setText(self,text):
        self.text = text
        self.update(0)
    
    def setColor(self,color):
        self.color = color
        self.update(0)
        
    def setPosition(self,position):
        self.position = position
        self.update(0)
    
    def update(self,time):
        self.image = self.font.render(self.text , 1, self.color).convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.center = self.position

class UIFrame(pygame.sprite.Sprite):
    def __init__(self,name,size=(800,600)):
        super(UIFrame, self).__init__()
        self.image = pygame.Surface(size)

        self.name = name
        self.position = (0,0)
        
        self.image.set_alpha(0)
        self.image = self.image.convert_alpha()
        
        self.topLeft = graphics.Image(graphics.ImageLoader().loadImage("data/graphics/window-01/window_top-left.png"))
        self.topRight = graphics.Image(graphics.ImageLoader().loadImage("data/graphics/window-01/window_top-right.png"))
        self.bottomLeft = graphics.Image(graphics.ImageLoader().loadImage("data/graphics/window-01/window_bottom-left.png"))
        self.bottomRight = graphics.Image(graphics.ImageLoader().loadImage("data/graphics/window-01/window_bottom-right.png"))
        
        self.rect = self.image.get_rect()        
        windowWidth,windowHeight=(self.rect.width,self.rect.height)
        
        self.image.blit(self.topLeft.getImage().convert_alpha(),(0,0))
        width,height = (self.topRight.getImage().get_rect().width,self.topRight.getImage().get_rect().height)
        self.image.blit(self.topRight.getImage().convert_alpha(),(windowWidth-width,0))
        
        width,height = (self.bottomLeft.getImage().get_rect().width,self.bottomLeft.getImage().get_rect().height)
        self.image.blit(self.bottomLeft.getImage().convert_alpha(),(0,windowHeight-height))

        width,height = (self.bottomRight.getImage().get_rect().width,self.bottomRight.getImage().get_rect().height)
        self.image.blit(self.bottomRight.getImage().convert_alpha(),(windowWidth-width,windowHeight-height))

class ClickerSprite(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.position = (0,0)
        self.rect = pygame.Rect(0,0,1,1)
        self.image = None
        
    def setPosition(self,position):
        self.position = position
        self.rect.center = position
        self.image = pygame.Surface((1,1))
        self.image.fill((255,255,255))
   
class StaticSprite(pygame.sprite.Sprite):
    
    def __init__(self, image, position = (0,0)):
        pygame.sprite.Sprite.__init__(self)
        self.position = position
        self.src_image = image
        self.direction = 0
        self.update(pygame.time.get_ticks())
        
    def update(self,current_t):
        if self.direction != 0:
            self.image = pygame.transform.rotate(self.src_image, self.direction)
        else:
            self.image = self.src_image
        self.rect = self.image.get_rect()
        self.rect.center = self.position

class AnimatedSprite(pygame.sprite.Sprite):
    
    def __init__(self, Image, position = (0,0)):
        super(AnimatedSprite, self).__init__()

        self.Image = Image
        self.position = position
        self.direction = 0
        self.update(pygame.time.get_ticks())
        
    def setPosition(self,position):
        self.position = position
        self.rect.center = self.position
        
    def update(self,current_t):
        self.src_image = self.Image.getImage()
        if self.direction != 0:
            self.image = pygame.transform.rotate(self.src_image, self.direction)
        else:
            self.image = self.src_image
        self.rect = self.image.get_rect()
        self.rect.center = self.position
        
        
