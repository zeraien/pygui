
"""
This will read HTML/XML pages and process them. It is so we can put HTML/XML into our fancy GUI!
I figure we can somehow create a nice system whereas we can design guis with XML or HTML.

For example:
<window title="A window">
    <skin bgcolor="255,255,255"/> <!-- all non-specified values go to default -->
    <hint>This is a window</hint>
    <panel>
        <image align="left" src="image.png"/>
        <text><b>Hello world!</b></text>
        <panel title="More data">
            <text>We are the masters of the universe.</text>
        </panel>
    </panel>
</window>
"""

import sgmllib

class MyParser(sgmllib.SGMLParser):
    "A simple parser class."

    def __init__(self):
        sgmllib.SGMLParser.__init__(self)
        self.__current_tag = None

    def parse(self, s):
        "Parse the given string 's'."
        self.feed(s)
        self.close()

    def start_window(self,attributes):
        self.__current_tag = "window"
        
    def start_skin(self,attributes):
        self.__current_tag = "skin"
        
    def start_hint(self,attributes):
        self.__current_tag = "hint"
        
    def start_panel(self,attributes):
        self.__current_tag = "panel"
        
    def start_text(self,attributes):
        self.__current_tag = "text"
        
    def start_bold(self,attributes):
        self.__current_tag = "bold"
        
    def start_italic(self,attributes):
        self.__current_tag = "italic"
        
    def start_underline(self,attributes):
        self.__current_tag = "underline"
        
    def start_image(self,attributes):
        self.__current_tag = "image"
        
    def handle_data(self,data):
        if self.__current_tag == "text":
            print data
        if self.__current_tag == "bold":
            print "BOLD:",data
        
    def end_window(self):
        self.__current_tag = None

    def end_skin(self):
        self.__current_tag = None

    def end_hint(self):
        self.__current_tag = None

    def end_panel(self):
        self.__current_tag = None

    def end_text(self):
        self.__current_tag = None

    def end_bold(self):
        self.__current_tag = None

    def end_italic(self):
        self.__current_tag = None

    def end_underline(self):
        self.__current_tag = None

    def end_image(self):
        self.__current_tag = None

f = open("data/layout.xml")
text = ""
for line in f.readlines():
    text = text + line
f.close()
p = MyParser()
p.parse(text)